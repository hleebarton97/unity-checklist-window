# Unity Checklist Window

Create multiple lists that can be toggled  
<img src='AddListsToggle.gif' alt='Adding Lists and Toggle' width=250>

Add tasks to lists and mark them as done  
<img src='AddListItems.gif' alt='Adding Lists and Toggle' width=250>

Clear everything when the checklist is no longer needed  
<img src='ClearLists.gif' alt='Adding Lists and Toggle' width=250>

## Developed by Lee Barton, Novasloth Games