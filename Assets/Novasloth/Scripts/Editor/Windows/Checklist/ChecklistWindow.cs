using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

// Lee Barton
namespace Novasloth {
    public class ChecklistWindow : EditorWindow {

        private const string ICON_PATH = "Packages/com.unity.collab-proxy/Editor/Assets/Icons/edited-file-dark.png";
        private const string CONTROL_NEW_LIST_NAME = "NewListControl";
        private const string EDITOR_PREFS_SAVE = "checklistList";

        private const string LABEL_CREATE_LIST = "Create New List";
        private const string BUTTON_CLEAR_LIST = "Clear All";

        [System.Serializable] private struct ChecklistListWrapper {
            public List<Checklist> checklistList;
        }

        private List<Checklist> checklistList = new List<Checklist>();
        private Queue<System.Action> eventsQueue = new Queue<System.Action>();

        private string newListTextArea = "";
        private bool clearListToggle = false;

        private bool doFocus = false;
        private string fieldToFocus = "";

        /////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        /////////////////////////////////////////////////////////////////

        [MenuItem("Window/Novasloth/Checklist")]
        public static void ShowWindow () {
            EditorWindow.GetWindow(typeof(ChecklistWindow));
        }

        private void OnEnable () {
            this.LoadChecklistList();
            Texture iconTexture = AssetDatabase.LoadAssetAtPath<Texture>(ICON_PATH);
            this.titleContent.image = iconTexture;
            this.titleContent.text = "Checklist";
        }

        private void OnGUI () {
            this.HandleKeyPress();

            this.DisplayNewListField();

            if (this.checklistList.Count > 0) {
                this.DisplayChecklist();
            }

            this.DisplayClearList();
        }

        private void Update () {
            while (this.eventsQueue.Count > 0) {
                this.eventsQueue.Dequeue().Invoke();
            }
        }

        private void OnLostFocus () {
            this.SaveChanges();
        }

        private void OnDestroy () {
            this.SaveChanges();
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        private void HandleKeyPress () {
            if (Event.current.isKey) {
                if (Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.KeypadEnter) {
                    string nameOfFocused = GUI.GetNameOfFocusedControl();
                    if (nameOfFocused == CONTROL_NEW_LIST_NAME) {
                        this.AddNewList();
                    } else {
                        foreach (Checklist list in this.checklistList) {
                            if (nameOfFocused == list.Title) {
                                this.AddChecklistItem(list);
                            }
                        }
                    }
                }
            }
        }

        private void AddNewList () {
            if (this.newListTextArea.Length > 0) {
                this.eventsQueue.Enqueue(() => {
                    this.checklistList.Add(new Checklist(this.newListTextArea));
                    this.newListTextArea = "";
                });
            }
        }

        private void AddChecklistItem (Checklist list) {
            if (list.NewItemTextField.Length > 0) {
                this.eventsQueue.Enqueue(() => {
                    list.ItemList.Add(
                        new ChecklistItem(list.NewItemTextField)
                    );
                    list.NewItemTextField = "";
                    this.fieldToFocus = list.Title;
                    this.doFocus = true;
                });
            }
        }

        private void DisplayNewListField () {
            Layout.LabelField(LABEL_CREATE_LIST, false);
            EditorGUILayout.BeginHorizontal(); {
                GUI.SetNextControlName(CONTROL_NEW_LIST_NAME);
                this.newListTextArea = Layout.TextField(this.newListTextArea);

                if (Layout.AddButton()) {
                    this.AddNewList();
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }

        private void DisplayChecklist () {
            foreach (Checklist list in this.checklistList) {
                EditorGUILayout.BeginHorizontal(); {
                    list.Toggle = EditorGUILayout.Foldout(list.Toggle, list.Title);

                    if (Layout.DeleteButton()) {
                        this.eventsQueue.Enqueue(() => {
                            this.checklistList.Remove(list);
                        });
                    }
                }
                EditorGUILayout.EndHorizontal();

                if (list.Toggle) {
                    this.DisplayChecklistItems(list);
                }
                EditorGUILayout.Space();
            }
        }

        private void DisplayChecklistItems (Checklist list) {
            foreach (ChecklistItem item in list.ItemList) {
                EditorGUILayout.BeginHorizontal(); {
                    item.Done = EditorGUILayout.Toggle(item.Done, GUILayout.MaxWidth(20));

                    if (item.Done) {
                        Layout.LabelField(item.Text, true);
                    } else {
                        item.Text = Layout.TextField(item.Text);
                    }

                    if (Layout.DeleteButton()) {
                        this.eventsQueue.Enqueue(() => {
                            list.ItemList.Remove(item);
                        });
                    }
                }
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.BeginHorizontal(); {
                GUI.SetNextControlName(list.Title);
                list.NewItemTextField = Layout.TextField(list.NewItemTextField);

                if (Layout.AddButton()) {
                    this.AddChecklistItem(list);
                }

                if (this.doFocus && this.fieldToFocus == list.Title) {
                    GUI.FocusControl(list.Title);
                    this.doFocus = false;
                    this.fieldToFocus = "";
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DisplayClearList () {
            if (this.checklistList.Count > 0) {
                EditorGUILayout.BeginHorizontal(); {
                    this.clearListToggle = EditorGUILayout.Toggle(this.clearListToggle, GUILayout.MaxWidth(20));
                    if (GUILayout.Button(BUTTON_CLEAR_LIST, EditorStyles.miniButton, GUILayout.MaxWidth(100)) && this.clearListToggle) {
                        this.checklistList.Clear();
                        this.clearListToggle = false;
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        public override void SaveChanges () {
            ChecklistListWrapper wrapper;
            wrapper.checklistList = this.checklistList;
            EditorPrefs.SetString(EDITOR_PREFS_SAVE, JsonUtility.ToJson(wrapper));
        }

        private void LoadChecklistList () {
            if (EditorPrefs.HasKey(EDITOR_PREFS_SAVE)) {
                this.checklistList = JsonUtility.FromJson<ChecklistListWrapper>(
                    EditorPrefs.GetString(EDITOR_PREFS_SAVE)
                ).checklistList;
            }
        }
    }
}
